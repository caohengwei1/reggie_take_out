package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.dto.DishDto;
import com.lovo.reggie.entity.Dish;

import java.util.List;

public interface DishService extends IService<Dish> {
    //新增菜品，同时插入菜品对应得口味数据
    void saveWithFlavor(DishDto dishDto);

    //根据ID查询菜品信息和对应的口味信息
    DishDto getByIdWithFlavor(Long id);

    //修改菜品信息，同时更新对应的口味信息
    void updateWithFlavor(DishDto dishDto);

    //菜品起售和停售（批量或单个）
    void status (Integer status, List<Long> ids);
    //通过id删除菜品(批量或者单个)
    void deleteByIdWithFlavor(List<Long> ids);

    /**
     * 菜品分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    Page<DishDto> pageList(int page, int pageSize, String name);

    /**
     * 根据条件查询对应的菜品数据
     * 手机端扩展了一些要查询的字段
     * @param dish
     * @return
     */
    List<DishDto> queryByCondition(Dish dish);
}
