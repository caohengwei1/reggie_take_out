package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.dto.OrdersDto;
import com.lovo.reggie.entity.Orders;

public interface OrdersService extends IService<Orders> {

    /**
     * 用户下单
     * @param orders
     */
    public void submit(Orders orders);

    /**
     * 用户订单分页查询
     * @param page
     * @param pageSize
     * @return
     */
    Page<OrdersDto> userPage(int page, int pageSize);

    /**
     * 后台系统的订单明细
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    Page<Orders> pageList(int page, int pageSize, String number, String beginTime, String endTime);

    /**
     * 再来一单
     * @param ordersId
     */
    void againSubmit(Long ordersId);
}
