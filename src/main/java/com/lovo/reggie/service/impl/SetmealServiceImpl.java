package com.lovo.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.reggie.common.CustomException;
import com.lovo.reggie.dto.DishDto;
import com.lovo.reggie.dto.SetmealDto;
import com.lovo.reggie.entity.Category;
import com.lovo.reggie.entity.Dish;
import com.lovo.reggie.entity.Setmeal;
import com.lovo.reggie.entity.SetmealDish;
import com.lovo.reggie.mapper.SetmealMapper;
import com.lovo.reggie.service.CategoryService;
import com.lovo.reggie.service.DishService;
import com.lovo.reggie.service.SetmealDishService;
import com.lovo.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper,Setmeal> implements SetmealService {
    @Autowired
    private SetmealDishService setmealDishService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private DishService dishService;
    /**
     * 新增套餐，同时需要保存套餐和菜品的关联关系
     * @param setmealDto
     */
    @Override
    @Transactional
    public void saveWithDish(SetmealDto setmealDto) {
        //保存套餐的基本信息，操作setmeal
        this.save(setmealDto);

        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //遍历设置套餐id
        setmealDishes = setmealDishes.stream().map((item)->{
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());

        //保存套餐和菜品的关联信息，操作setmeal_dish
        setmealDishService.saveBatch(setmealDishes);
    }
    /**
     * 删除套餐（批量或者单个），同时删除套餐和菜品的关联数据
     * @param ids
     */
    @Override
    @Transactional//事务
    public void deleteByIdsWithDish(List<Long> ids) {
        //查询套餐状态，确定是否可以删除
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.in(ids!=null,Setmeal::getId,ids);
        queryWrapper.eq(Setmeal::getStatus,1);
        int count = this.count(queryWrapper);
        if (count>0){
            //如果不能删除，抛出一个业务异常
            throw new CustomException("套餐正在售卖中，不能删除");
        }
        //如果可以删除，先删除套餐里的数据——setmeal
        this.removeByIds(ids);

        LambdaQueryWrapper<SetmealDish> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.in(SetmealDish::getSetmealId,ids);
        //删除关系表里的数据——setmeal_dish
        setmealDishService.remove(queryWrapper1);
    }
    /**
     * 套餐起售和停售（批量或单个）
     * @param status
     * @param ids
     */
    @Override
    public void updateStatus(Integer status, List<Long> ids) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.in(ids != null,Setmeal::getId,ids);

        List<Setmeal> setmeals = this.list(queryWrapper);
        for (Setmeal setmeal : setmeals) {
            if (setmeal!=null&&status!=null){
                setmeal.setStatus(status);
                this.updateById(setmeal);
            }
        }
    }
    /**
     * 根据ID查询套餐信息和关联菜品信息
     * @param id
     * @return
     */
    @Override
    public SetmealDto getDataById(Long id) {
        SetmealDto setmealDto = new SetmealDto();
        //查询套餐的基本信息
        Setmeal setmeal = this.getById(id);
        //把套餐信息拷贝到setmealDto
        BeanUtils.copyProperties(setmeal,setmealDto);

        //查询套餐关联的菜品信息
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,setmeal.getId());
        List<SetmealDish> setmealDishes = setmealDishService.list(queryWrapper);
        //setmealDishes传到setmealDto对象里
        setmealDto.setSetmealDishes(setmealDishes);
        return setmealDto;
    }
    /**
     * 修改套餐信息,同时更新绑定的菜品信息
     * @param setmealDto
     */
    @Override
    public void updateWithDish(SetmealDto setmealDto) {
        //先修改套餐的信息(更新setmeal表信息)
        this.updateById(setmealDto);

        //修改绑定的菜品信息
        //先清理当前绑定的菜品
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,setmealDto.getId());
        setmealDishService.remove(queryWrapper);

        //添加当前提交过来的要绑定菜品信息
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        //遍历添加
        setmealDishes = setmealDishes.stream().map((item)->{
            item.setId(null);
            item.setIsDeleted(0);
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());

        //保存菜品信息到数据库
        setmealDishService.saveBatch(setmealDishes);
    }
    /**
     * 套餐分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    public Page<SetmealDto> getPageList(int page, int pageSize, String name) {
        //分页构造器
        Page<Setmeal> pageInfo = new Page<>(page,pageSize);
        Page<SetmealDto> setmealDtoPage = new Page<>();
        //条件构造器
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        //查询条件（根据套餐名字模糊查询）
        queryWrapper.like(StringUtils.isNotEmpty(name),Setmeal::getName,name);
        //排序条件(根据更新时间)
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        //执行分页查询
        this.page(pageInfo,queryWrapper);

        //因为页面展示的数据里有套餐的分类名称
        //对象拷贝
        BeanUtils.copyProperties(pageInfo,setmealDtoPage,"records");

        //得到Setmeal分页对象里的所有Setmeal对象集合
        List<Setmeal> setmeals = pageInfo.getRecords();
        List<SetmealDto> list = setmeals.stream().map((item)->{
            SetmealDto setmealDto = new SetmealDto();
            //拷贝setmeal对象到setmealDto里去
            BeanUtils.copyProperties(item,setmealDto);
            //根据id查询分类对象
            Category category = categoryService.getById(item.getCategoryId());
            if (category!=null){
                //需要设置分类对象的名称
                setmealDto.setCategoryName(category.getName());
            }
            return setmealDto;
        }).collect(Collectors.toList());

        //把SetmealDto对象集合加入到分页集合里去
        setmealDtoPage.setRecords(list);
        return setmealDtoPage;
    }
    /**
     * 根据条件查询套餐数据
     * @param setmeal
     * @return
     */
    @Override
    public List<Setmeal> queryByCondition(Setmeal setmeal) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(setmeal.getCategoryId()!=null,Setmeal::getCategoryId,setmeal.getCategoryId());
        queryWrapper.eq(setmeal.getStatus()!=null,Setmeal::getStatus,setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> setmeals = this.list(queryWrapper);
        return setmeals;
    }
    /**
     * 查询套餐里的菜品信息（包含菜品的份数）
     * @param setmealId
     * @return
     */
    @Override
    public List<DishDto> queryAndDish(Long setmealId) {
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(setmealId!=null,SetmealDish::getSetmealId,setmealId);
        //查出套餐里的所有菜品（SetmealDish关系表）
        List<SetmealDish> setmealDishList = setmealDishService.list(queryWrapper);

        List<DishDto> dishDtoList = setmealDishList.stream().map((item)->{
            DishDto dishDto = new DishDto();
            //设置菜品的份数
            dishDto.setCopies(item.getCopies());
            //查出菜品信息
            Dish dish = dishService.getById(item.getDishId());
            log.info("查出菜品信息:{}",dish);
            //拷贝到dishDto对象里去
            BeanUtils.copyProperties(dish,dishDto);
            return dishDto;
        }).collect(Collectors.toList());
        return dishDtoList;
    }
}
