package com.lovo.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.reggie.entity.SetmealDish;
import com.lovo.reggie.mapper.SetmealDishMapper;
import com.lovo.reggie.mapper.SetmealMapper;
import com.lovo.reggie.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper, SetmealDish> implements SetmealDishService {
}
