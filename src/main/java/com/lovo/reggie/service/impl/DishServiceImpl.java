package com.lovo.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.reggie.common.CustomException;
import com.lovo.reggie.dto.DishDto;
import com.lovo.reggie.entity.*;
import com.lovo.reggie.mapper.DishMapper;
import com.lovo.reggie.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper,Dish> implements DishService {
    @Autowired
    private DishFlavorService dishFlavorService;
    @Autowired
    private SetmealDishService setmealDishService;
    @Autowired
    private SetmealService setmealService;
    @Autowired
    private CategoryService categoryService;

    /**
     * 新增菜品，同时保存对应得口味数据
     * @param dishDto
     */
    @Override
    @Transactional//事务处理
    public void saveWithFlavor(DishDto dishDto) {
        dishDto.setIsDeleted(0);
        //保存菜品得基本信息到菜品表dish
        this.save(dishDto);

        Long dishId = dishDto.getId();//菜品ID

        List<DishFlavor> flavors = dishDto.getFlavors();
        //map将字符串转为对象接收
        flavors = flavors.stream().map((item)->{
            item.setIsDeleted(0);
            item.setDishId(dishId);
            return item;
        }).collect(Collectors.toList());
        //保存菜品口味数据到菜品口味表dish_flavor
        dishFlavorService.saveBatch(flavors);
    }

    /**
     * 根据ID查询菜品信息和对应的口味信息
     * @param id
     * @return
     */
    @Override
    public DishDto getByIdWithFlavor(Long id) {
        DishDto dishDto = new DishDto();

        //查询菜品基本信息，从dish表查询
        Dish dish = this.getById(id);
        //拷贝对象
        BeanUtils.copyProperties(dish,dishDto);
        //查询菜品对应的口味信息，从dishFlavor表查询
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dish.getId());
        List<DishFlavor> flavors = dishFlavorService.list(queryWrapper);
        //flavors传到dishDto对象里
        dishDto.setFlavors(flavors);
        return dishDto;
    }

    /**
     * 修改菜品信息，同时更新对应的口味信息
     * @param dishDto
     */
    @Override
    @Transactional//事务处理
    public void updateWithFlavor(DishDto dishDto) {
        //修改菜品信息，更新dish表信息
        this.updateById(dishDto);

        //修改口味表信息
        //先清理当前的口味数据——dishFlavor表的delete操作
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dishDto.getId());
        dishFlavorService.remove(queryWrapper);

        //添加当前提交过来的口味数据——dishFlavor表的insert操作

        List<DishFlavor> flavors = dishDto.getFlavors();
        //map遍历
        flavors = flavors.stream().map((item)->{
            //因为有逻辑删除的操作，因此删除后，删除列为1，在添加的话会导致口味的id为重复的，会报错
            // 因此在添加数据时将id设置为null，每次它会使用雪花算法，自动生成新的id
            item.setId(null);
            item.setIsDeleted(0);
            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());
        //保存菜品口味数据到菜品口味表dish_flavor
        dishFlavorService.saveBatch(flavors);
    }

    /**
     * 菜品起售和停售（批量或单个）
     * @param status
     * @param ids
     */
    @Override
    public void status(Integer status, List<Long> ids) {
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.in(ids != null,Dish::getId,ids);

        List<Dish> list = this.list(queryWrapper);
        for (Dish dish : list) {
            if (dish!=null){
                dish.setStatus(status);
                this.updateById(dish);
            }
        }
    }

    /**
     * 通过id删除菜品
     * @param ids
     */
    @Override
    @Transactional
    public void deleteByIdWithFlavor(List<Long> ids) {
        //先删除菜品信息
        //查询菜品状态，确定是否可以删除
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ids!=null,Dish::getId,ids);
        queryWrapper.eq(Dish::getStatus,1);
        int count = this.count(queryWrapper);
        //判断有否有正在售卖的菜品
        if (count>0) {
            //不能删除，抛出自定义异常
            throw new CustomException("有菜品正在售卖，无法删除");
        }
        //判断当前菜品是否在套餐中，且查询套餐状态，确定是否可以删除
        LambdaQueryWrapper<SetmealDish> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.in(ids!=null,SetmealDish::getDishId,ids);
        //查出套餐里关联的所有菜品
        List<SetmealDish> setmealDishList = setmealDishService.list(queryWrapper1);
        //遍历菜品，根据菜品id找到对应套餐，看套餐是否在售
        LambdaQueryWrapper<Setmeal> queryWrapper2 = new LambdaQueryWrapper<>();
        List<Long> setmealIds = new ArrayList<>();
        if (setmealDishList.size()!=0){
            for (SetmealDish setmealDish : setmealDishList) {
                setmealIds.add(setmealDish.getSetmealId());
            }
            queryWrapper2.in(setmealIds.size()!=0,Setmeal::getId,setmealIds);
            queryWrapper2.eq(Setmeal::getStatus,1);
            int count1 = setmealService.count(queryWrapper2);
            //判断是否有正在售卖的套餐
            if (count1>0){
                //不能删除，抛出异常
                throw new CustomException("该菜品关联的套餐正在售卖，无法删除");
            }
        }
        //如果可以删除，先删除菜品里的数据Dish
        this.removeByIds(ids);
        //删除口味信息
        LambdaQueryWrapper<DishFlavor> queryWrapper3 = new LambdaQueryWrapper<>();
        queryWrapper3.in(ids!=null,DishFlavor::getDishId,ids);
        dishFlavorService.remove(queryWrapper3);
    }

    @Override
    public Page<DishDto> pageList(int page, int pageSize, String name) {
        //分页构造器
        Page<Dish> pageInfo = new Page<>(page,pageSize);
        Page<DishDto>dishDtoPage = new Page<>();
        //条件构造器
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        //添加查询条件
        queryWrapper.like(StringUtils.isNotEmpty(name),Dish::getName,name);
        //添加排序条件
        queryWrapper.orderByDesc(Dish::getUpdateTime);
        //执行分页查询
        this.page(pageInfo,queryWrapper);

        //对象拷贝(被拷贝对象，拷贝对象，忽略属性)
        //records:就是这个page集合里的对象集合，因为我们要返回的是带有菜品分类名字的DishDto类型的对象集合，所以先不拷贝这个属性，下面做处理
        BeanUtils.copyProperties(pageInfo,dishDtoPage,"records");

        //得到Dish的分页数据里的Dish对象集合
        List<Dish> records = pageInfo.getRecords();
        //DishDto对象的集合
        List<DishDto> list = records.stream().map((item)->{
            DishDto dishDto = new DishDto();
            //拷贝dish集合里的数据给dishDto对象
            BeanUtils.copyProperties(item,dishDto);
            Long categoryId = item.getCategoryId();//得到分类id
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);
            if (category!=null){
                //得到菜品分类的名字
                String categoryName = category.getName();
                //设置到dishDto对象里
                dishDto.setCategoryName(categoryName);
            }

            //返回一个dishDto对象
            return dishDto;
            //将返回的disDto对象收集成List集合
        }).collect(Collectors.toList());

        //将DishDto对象的集合设置到DishDto对象的分页集合里去
        dishDtoPage.setRecords(list);
        return dishDtoPage;
    }

    @Override
    public List<DishDto> queryByCondition(Dish dish) {
        //构造查询条件
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper();
        //根据id查
        queryWrapper.eq(dish.getCategoryId()!=null,Dish::getCategoryId,dish.getCategoryId());
        //根据菜品名称查
        queryWrapper.like(dish.getName()!=null,Dish::getName,dish.getName());
        //添加条件，查询状态为1的（起售状态）的菜品
        queryWrapper.eq(Dish::getStatus,1);
        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        List<Dish> dishList = this.list(queryWrapper);

        //DishDto对象的集合
        List<DishDto> dishDtoList = dishList.stream().map((item)->{
            DishDto dishDto = new DishDto();
            //拷贝dish集合里的数据给dishDto对象
            BeanUtils.copyProperties(item,dishDto);
            Long categoryId = item.getCategoryId();//得到分类id
            //根据id查询分类对象
            Category category = categoryService.getById(categoryId);
            if (category!=null){
                //得到菜品分类的名字
                String categoryName = category.getName();
                //设置到dishDto对象里
                dishDto.setCategoryName(categoryName);
            }
            //当前菜品的id
            Long dishId = item.getId();
            LambdaQueryWrapper<DishFlavor> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.eq(DishFlavor::getDishId,dishId);
            List<DishFlavor> dishFlavorList = dishFlavorService.list(queryWrapper1);
            dishDto.setFlavors(dishFlavorList);
            //返回一个dishDto对象
            return dishDto;
            //将返回的disDto对象收集成List集合
        }).collect(Collectors.toList());
        return dishDtoList;
    }
}
