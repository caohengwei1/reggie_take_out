package com.lovo.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lovo.reggie.common.BaseContext;
import com.lovo.reggie.common.CustomException;
import com.lovo.reggie.dto.OrdersDto;
import com.lovo.reggie.entity.*;
import com.lovo.reggie.mapper.OrdersMapper;
import com.lovo.reggie.service.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Slf4j
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper,Orders> implements OrdersService {
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private UserService userService;
    @Autowired
    private AddressBookService addressBookService;
    @Autowired
    private OrderDetailService orderDetailService;

    /**
     * 用户下单
     * @param orders
     */
    @Override
    @Transactional//事务
    public void submit(Orders orders) {
        //当前用户
        Long userId = BaseContext.getCurrentId();

        //查询当前用户的购物车数据
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(ShoppingCart::getUserId,userId);
        List<ShoppingCart> shoppingCartList = shoppingCartService.list(queryWrapper);

        if (shoppingCartList == null || shoppingCartList.size() == 0){
            throw new CustomException("购物车为空,不能下单");
        }
        //查询用户数据
        User user = userService.getById(userId);

        //查询地址数据
        AddressBook addressBook = addressBookService.getById(orders.getAddressBookId());
        if (addressBook == null){
            throw new CustomException("用户地址信息有误，不能下单");
        }

        //生成订单号
//        这个idwoker所生成的id是自增长的。
        long orderId = IdWorker.getId();

        //原子操作
        //在高并发，多线程的情况下也会计算正确
        //保证线程安全
        AtomicInteger amount = new AtomicInteger(0);

        List<OrderDetail> orderDetailList = shoppingCartList.stream().map((item)->{
            OrderDetail orderDetail = new OrderDetail();
            orderDetail.setOrderId(orderId);
            orderDetail.setNumber(item.getNumber());
            orderDetail.setDishFlavor(item.getDishFlavor());
            orderDetail.setDishId(item.getDishId());
            orderDetail.setSetmealId(item.getSetmealId());
            orderDetail.setName(item.getName());
            orderDetail.setImage(item.getImage());
            orderDetail.setAmount(item.getAmount());
            //AtomicInteger对象的累加操作
            //multiply：乘法
            amount.addAndGet(item.getAmount().multiply(new BigDecimal(item.getNumber())).intValue());
            return orderDetail;
        }).collect(Collectors.toList());
        //下单：向订单表插入数据（一条数据）
        orders.setNumber(String.valueOf(orderId));//订单号
        orders.setId(orderId);//订单id
        orders.setOrderTime(LocalDateTime.now());//订单时间
        orders.setCheckoutTime(LocalDateTime.now());//结账时间
        orders.setStatus(2);//订单状态（待派送）
        orders.setAmount(new BigDecimal(amount.get()));//订单总金额
        orders.setUserId(userId);//用户id
        orders.setUserName(user.getName());//用户名
        orders.setConsignee(addressBook.getConsignee());//收货人
        orders.setPhone(addressBook.getPhone());//收货人手机号
        orders.setAddress((addressBook.getProvinceName()==null?"":addressBook.getProvinceName())
                +(addressBook.getCityName()==null?"":addressBook.getCityName())
                +(addressBook.getDistrictName()==null?"":addressBook.getDistrictName())
                +(addressBook.getDetail()==null?"":addressBook.getDetail()));//地址信息

        this.save(orders);

        //向明细表插入数据（多条数据）
        orderDetailService.saveBatch(orderDetailList);

        //清空购物车数据
        shoppingCartService.remove(queryWrapper);

    }

    /**
     * 用户订单分页查询
     * @param page
     * @param pageSize
     * @return
     */
    @Override
    public Page<OrdersDto> userPage(int page, int pageSize) {
        //分页构造器
        Page<Orders> pageInfo = new Page<>(page,pageSize);
        Page<OrdersDto> dtoPage = new Page<>();

        //查询条件
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        //这里是直接把当前用户分页的全部结果查询出来，要添加用户id作为查询条件，否则会出现用户可以查询到其他用户的订单情况
        queryWrapper.eq(Orders::getUserId,BaseContext.getCurrentId());
        //根据更新时间降序排序
        queryWrapper.orderByDesc(Orders::getOrderTime);
        //查出订单分页
        this.page(pageInfo,queryWrapper);

        //通过订单id查询对应的菜品/套餐
        //得到订单集合
        List<Orders> records = pageInfo.getRecords();

        List<OrdersDto> ordersDtos = records.stream().map((item)->{
            OrdersDto ordersDto = new OrdersDto();

            //获取订单id
            Long ordersId = item.getId();
            List<OrderDetail> orderDetailList = this.getorderDetailListById(ordersId);
            //拷贝
            BeanUtils.copyProperties(item,ordersDto);
            ordersDto.setOrderDetails(orderDetailList);
            return ordersDto;
        }).collect(Collectors.toList());

        BeanUtils.copyProperties(pageInfo,dtoPage,"records");
        dtoPage.setRecords(ordersDtos);

        return dtoPage;
    }

    /**
     * 后台系统的订单明细
     * @param page
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @Override
    public Page<Orders> pageList(int page, int pageSize, String number, String beginTime, String endTime) {
        //分页构造器
        Page<Orders> pageInfo = new Page<>(page,pageSize);

        //给条件查询
        //条件构造器
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(number),Orders::getNumber,number)
                .gt(StringUtils.isNotEmpty(beginTime),Orders::getOrderTime,beginTime)
                .lt(StringUtils.isNotEmpty(endTime),Orders::getOrderTime,endTime);
        this.page(pageInfo,queryWrapper);
        return pageInfo;
    }

    /**
     * 再来一单
     */
    @Override
    @Transactional
    public void againSubmit(Long ordersId) {
        LambdaQueryWrapper<OrderDetail> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderDetail::getOrderId,ordersId);
        //根据订单id查到订单关联的菜品/套餐(订单明细)
        List<OrderDetail> orderDetailList = orderDetailService.list(queryWrapper);

        //通过用户id把原来购物车清空了(避免数据有问题)
        shoppingCartService.clean();

        //将这些菜品/套餐传到购物车
        //用户id
        Long userId = BaseContext.getCurrentId();

        List<ShoppingCart> shoppingCarts = orderDetailList.stream().map((item)->{
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.setUserId(userId);
            shoppingCart.setImage(item.getImage());
            Long dishId = item.getDishId();
            Long setmealId = item.getSetmealId();
            //判断是菜品还是套餐
            if (dishId!=null){
                //是菜品
                shoppingCart.setDishId(dishId);
            }else {
                //是套餐
                shoppingCart.setSetmealId(setmealId);
            }
            shoppingCart.setName(item.getName());
            shoppingCart.setDishFlavor(item.getDishFlavor());
            shoppingCart.setNumber(item.getNumber());
            shoppingCart.setAmount(item.getAmount());
            shoppingCart.setCreateTime(LocalDateTime.now());
            return shoppingCart;
        }).collect(Collectors.toList());

        //保存到购物车里(批量保存:saveBatch)
        shoppingCartService.saveBatch(shoppingCarts);
    }

    /**
     * 根据id获取OrderDetail订单明细的集合信息
     * 这里抽离出来是为了避免在stream中遍历的时候直接使用构造条件来查询导致eq叠加，从而导致后面查询的数据都是null
     * @param id
     * @return
     */
    public List<OrderDetail> getorderDetailListById(Long id){
        LambdaQueryWrapper<OrderDetail> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(OrderDetail::getOrderId,id);
        return orderDetailService.list(queryWrapper);
    }
}
