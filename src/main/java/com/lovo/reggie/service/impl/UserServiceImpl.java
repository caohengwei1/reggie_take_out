package com.lovo.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.lovo.reggie.entity.User;
import com.lovo.reggie.mapper.UserMapper;
import com.lovo.reggie.service.UserService;

import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService{
}
