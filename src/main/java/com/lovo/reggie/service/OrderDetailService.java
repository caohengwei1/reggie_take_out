package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
