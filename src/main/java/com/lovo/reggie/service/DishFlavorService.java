package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}
