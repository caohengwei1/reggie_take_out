package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.entity.Category;

import java.util.List;

public interface CategoryService extends IService<Category> {
    /**
     * 根据id删除分类
     * @param ids
     */
    public void remove(Long ids);

    /**
     * 分类信息分页查询
     * @return
     */
    Page<Category> pageList(int page,int pageSize);

    /**
     * 根据条件查询分类数据
     * @param category
     * @return
     */
    List<Category> queryByCondition(Category category);
}
