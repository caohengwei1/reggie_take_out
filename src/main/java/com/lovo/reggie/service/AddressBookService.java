package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.common.R;
import com.lovo.reggie.entity.AddressBook;

import java.util.List;

public interface AddressBookService extends IService<AddressBook> {
    /**
     * 设置默认地址
     * @param addressBook
     */
    void setDefault(AddressBook addressBook);

    /**
     * 查询指定用户的全部地址
     * @param addressBook
     */
    List<AddressBook> getAll(AddressBook addressBook);
}
