package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.entity.Employee;

public interface EmployeeService extends IService<Employee> {
    /**
     * 员工信息分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    Page<Employee> pageList(int page, int pageSize, String name);
}
