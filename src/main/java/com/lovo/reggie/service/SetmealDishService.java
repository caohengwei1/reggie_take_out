package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
