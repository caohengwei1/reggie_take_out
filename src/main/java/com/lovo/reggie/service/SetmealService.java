package com.lovo.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lovo.reggie.dto.DishDto;
import com.lovo.reggie.dto.SetmealDto;
import com.lovo.reggie.entity.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {
    /**
     * 新增套餐，同时需要保存套餐和菜品的关联关系
     * @param setmealDto
     */
    public void saveWithDish(SetmealDto setmealDto);

    /**
     * 删除套餐（批量或者单个），同时删除套餐和菜品的关联数据
     * @param ids
     */
    public void deleteByIdsWithDish(List<Long> ids);

    /**
     * 套餐起售和停售（批量或单个）
     * @param status
     * @param ids
     */
    public void updateStatus(Integer status, List<Long> ids);

    /**
     * 根据ID查询套餐信息和关联菜品信息
     * @param id
     * @return
     */
    SetmealDto getDataById(Long id);

    /**
     * 修改套餐信息,同时更新绑定的菜品信息
     * @param setmealDto
     */
    void updateWithDish(SetmealDto setmealDto);

    /**
     * 套餐分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    Page<SetmealDto> getPageList(int page, int pageSize, String name);

    /**
     * 根据条件查询套餐数据
     * @param setmeal
     * @return
     */
    List<Setmeal> queryByCondition(Setmeal setmeal);

    /**
     * 查询套餐里的菜品信息（包含菜品的份数）
     * @param setmealId
     * @return
     */
    List<DishDto> queryAndDish(Long setmealId);
}
