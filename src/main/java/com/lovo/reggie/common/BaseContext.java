package com.lovo.reggie.common;

/**
 * 基于ThreadLocal封装工具类，用户保存和获取当前登录用户id
 */
public class BaseContext {
    /*
    因为客户端发送的每次http请求，对应的在服务端都会分配
    一个新的线程来处理，因此在方法里都属于同一个线程
     */
    /**
     * ThreadLocal是Thread的局部变量，当使用ThreadLocal维护变量时，ThreadLocal为每个使用该变量的线程提供独立的变量副本，
     * 所以每一个线程都可以独立的改变自己的副本，且不会影响到其它线程所对应的副本。
     */
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 设置值
     * @param id
     */
    public static void setCurrentId(Long id){
        //ThreadLocal：常用方法set(T value)：设置当前线程的线程局部变量的值
        threadLocal.set(id);
    }

    /**
     * 获取值
     * @return
     */
    public static Long getCurrentId(){
        //ThreadLocal：常用方法get()：返回当前线程所对应的线程局部变量的值
        return threadLocal.get();
    }
}
