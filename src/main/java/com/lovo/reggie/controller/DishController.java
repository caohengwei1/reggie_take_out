package com.lovo.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lovo.reggie.common.R;
import com.lovo.reggie.dto.DishDto;
import com.lovo.reggie.entity.Category;
import com.lovo.reggie.entity.Dish;
import com.lovo.reggie.entity.DishFlavor;
import com.lovo.reggie.service.CategoryService;
import com.lovo.reggie.service.DishFlavorService;
import com.lovo.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 菜品管理
 */
@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {
    @Autowired
    private DishService dishService;

    /**
     * 新增菜品
     * @param dishDto
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody DishDto dishDto){
        log.info("新增菜品:{}",dishDto.toString());
        dishService.saveWithFlavor(dishDto);
        return R.success("新增菜品成功！");
    }

    /**
     * 菜品分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name){
        log.info("菜品分页查询...");
        Page<DishDto>dishDtoPage = dishService.pageList(page,pageSize,name);
        return R.success(dishDtoPage);
    }

    /**
     * 根据ID查询菜品信息和对应的口味信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<DishDto> getById(@PathVariable Long id){
        log.info("根据ID查询菜品信息和对应的口味信息...");
//        两张表，需要在service里扩展方法
        DishDto dishDto = dishService.getByIdWithFlavor(id);
        return R.success(dishDto);
    }

    /**
     * 修改菜品信息，同时更新对应的口味信息
     * @param dishDto
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto){
        log.info("修改菜品信息，同时更新对应的口味信息...");
        //要更新两张表的数据
        dishService.updateWithFlavor(dishDto);
        return R.success("修改菜品信息成功！");
    }

    /**
     * 菜品起售和停售（批量或单个）
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    public R<String> status(@PathVariable("status") Integer status,@RequestParam List<Long> ids){
        log.info("菜品起售和停售...");
        log.info("status:{}",status);
        log.info("ids:{}",ids);
        dishService.status(status,ids);
        return R.success("菜品起售停售状态修改成功！");
    }

    /**
     * 通过id删除菜品（批量或单个）
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> deleteById(@RequestParam List<Long> ids){
        log.info("通过id删除菜品...");
        log.info("ids:{}",ids);
        dishService.deleteByIdWithFlavor(ids);
        return R.success("删除菜品成功！");
    }

    /**
     * 根据条件查询对应的菜品数据
     * @return
     */
    /*@GetMapping("/list")
    public R<List<Dish>> list(Dish dish){
        //构造查询条件
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper();
        //根据id查
        queryWrapper.eq(dish.getCategoryId()!=null,Dish::getCategoryId,dish.getCategoryId());
        //根据菜品名称查
        queryWrapper.like(dish.getName()!=null,Dish::getName,dish.getName());
        //添加条件，查询状态为1的（起售状态）的菜品
        queryWrapper.eq(Dish::getStatus,1);
        //添加排序条件
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);
        List<Dish> dishList = dishService.list(queryWrapper);

        return R.success(dishList);
    }*/

    /**
     * 根据条件查询对应的菜品数据
     * 手机端扩展了一些要查询的字段
     * @param dish
     * @return
     */
    @GetMapping("/list")
    public R<List<DishDto>> list(Dish dish){
        List<DishDto> dishDtoList = dishService.queryByCondition(dish);
        return R.success(dishDtoList);
    }
}
