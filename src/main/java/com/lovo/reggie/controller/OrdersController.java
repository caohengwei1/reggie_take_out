package com.lovo.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lovo.reggie.common.BaseContext;
import com.lovo.reggie.common.R;
import com.lovo.reggie.dto.OrdersDto;
import com.lovo.reggie.entity.OrderDetail;
import com.lovo.reggie.entity.Orders;
import com.lovo.reggie.entity.ShoppingCart;
import com.lovo.reggie.service.OrderDetailService;
import com.lovo.reggie.service.OrdersService;
import com.lovo.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 订单
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrdersController {
    @Autowired
    private OrdersService ordersService;

    /**
     * 用户下单
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){
        log.info("用户下单...");
        ordersService.submit(orders);
        return R.success("下单成功！");
    }

    /**
     * 用户订单分页查询
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public R<Page> userPage(int page,int pageSize){
        log.info("用户订单分页查询...");
        Page<OrdersDto> ordersDtoPage = ordersService.userPage(page,pageSize);
        return R.success(ordersDtoPage);
    }

    /**
     * 后台系统的订单明细
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String number,String beginTime,String endTime){
        log.info("后台系统订单分页明细...");
        Page<Orders> ordersPage = ordersService.pageList(page,pageSize,number,beginTime,endTime);
        return R.success(ordersPage);
    }

    /**
     * 更改订单派送状态
     * @param map
     * @return
     */
    @PutMapping
    public R<String> updateStatus(@RequestBody Map<String,String>map){
        log.info("更改订单派送状态...");
        String id = map.get("id");
        //字符串转Long类型
        Long orderId = Long.parseLong(id);
        Integer status = Integer.parseInt(map.get("status"));

        if (orderId==null||status==null){
            return R.error("传入的参数不合法");
        }
        Orders orders = ordersService.getById(orderId);
        orders.setStatus(status);

        ordersService.updateById(orders);
        return R.success("更改订单派送状态成功!");
    }

    /**
     * 再来一单
     * @param map
     * @return
     */
    @PostMapping("/again")
    public R<String> againSubmit(@RequestBody Map<String,String>map){
        String id = map.get("id");
        Long ordersId = Long.parseLong(id);
        ordersService.againSubmit(ordersId);
        return R.success("再来一单成功!");
    }

}
