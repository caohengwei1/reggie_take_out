package com.lovo.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lovo.reggie.common.R;
import com.lovo.reggie.dto.DishDto;
import com.lovo.reggie.dto.SetmealDto;
import com.lovo.reggie.entity.Category;
import com.lovo.reggie.entity.Dish;
import com.lovo.reggie.entity.Setmeal;
import com.lovo.reggie.entity.SetmealDish;
import com.lovo.reggie.service.CategoryService;
import com.lovo.reggie.service.DishService;
import com.lovo.reggie.service.SetmealDishService;
import com.lovo.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.net.QCodec;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 套餐管理
 */
@RestController
@RequestMapping("/setmeal")
@Slf4j
public class SetmealController {
    @Autowired
    private SetmealService setmealService;

    /**
     * 新增套餐
     * @return
     */
    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto){
        log.info("新增套餐信息：{}",setmealDto);
        setmealService.saveWithDish(setmealDto);
        return R.success("新增套餐成功！");
    }

    /**
     * 套餐分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<Page> page(int page,int pageSize,String name){
        log.info("套餐分页查询...");
        Page<SetmealDto> setmealDtoPage= setmealService.getPageList(page,pageSize,name);
        return R.success(setmealDtoPage);
    }

    /**
     * 套餐起售和停售（批量或单个）
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    public R<String> status(@PathVariable("status") Integer status,@RequestParam List<Long> ids){
        log.info("套餐起售和停售...");
        log.info("status:{}",status);
        log.info("ids:{}",ids);
        setmealService.updateStatus(status,ids);
        return R.success("套餐起售停售状态修改成功！");
    }

    /**
     * 删除套餐（批量或者单个）
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> delete(@RequestParam List<Long> ids){
        log.info("通过id删除套餐...");
        log.info("ids:{}",ids);
        setmealService.deleteByIdsWithDish(ids);
        return R.success("删除成功！");
    }

    /**
     * 根据ID查询套餐信息和关联菜品信息
     * @return
     */
    @GetMapping("/{id}")
    public R<SetmealDto> getById(@PathVariable Long id){
        log.info("根据ID查询套餐信息...");
        SetmealDto setmealDto = setmealService.getDataById(id);
        return R.success(setmealDto);
    }

    /**
     * 修改套餐信息,同时更新绑定的菜品信息
     * @param setmealDto
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody SetmealDto setmealDto){
        log.info("修改套餐信息，同时更新绑定的菜品信息...");
        //修改信息
        setmealService.updateWithDish(setmealDto);
        return R.success("修改套餐信息成功！");
    }

    /**
     * 根据条件查询套餐数据
     * @param setmeal
     * @return
     */
    @GetMapping("/list")
    public R<List<Setmeal>> list(Setmeal setmeal){
        List<Setmeal> setmeals = setmealService.queryByCondition(setmeal);
        return R.success(setmeals);
    }

    /**
     *查询套餐里的菜品信息（包含菜品的份数）
     * @param setmealId
     * @return
     */
    @GetMapping("/dish/{id}")
    public R<List<DishDto>> dish(@PathVariable("id") Long setmealId){
        List<DishDto> dishDtoList = setmealService.queryAndDish(setmealId);
        return R.success(dishDtoList);
    }

}
