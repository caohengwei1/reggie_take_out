package com.lovo.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lovo.reggie.common.BaseContext;
import com.lovo.reggie.common.R;
import com.lovo.reggie.entity.ShoppingCart;
import com.lovo.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 购物车
 */
@Slf4j
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加购物车
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart){
        log.info("添加购物车：{}",shoppingCart);
        //设置用户id，指定当前是哪个用户的购物车数据
        Long userId = BaseContext.getCurrentId();
        shoppingCart.setUserId(userId);

        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,userId);

        //查询当前添加的菜品或者套餐是否在购物车里
        //获取菜品id
        Long dishId = shoppingCart.getDishId();
        if (dishId!=null){
            //添加到购物车的是菜品
            queryWrapper.eq(ShoppingCart::getDishId,dishId);
        }else {
            //添加到购物车的是套餐
            queryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }

        ShoppingCart shopping = shoppingCartService.getOne(queryWrapper);
        if (shopping!=null){
            //如果已经存在，在原来数量基础上加1
            //原先数量
            Integer number = shopping.getNumber();
            shopping.setNumber(number+1);
            shoppingCartService.updateById(shopping);
        }else {
            //如果不存在，则添加购物车，数量默认为1
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartService.save(shoppingCart);
            shopping = shoppingCart;
        }
        return R.success(shopping);
    }

    /**
     * 查询购物车
     * @return
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> list(){
        log.info("查看购物车...");

        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());
        queryWrapper.orderByAsc(ShoppingCart::getCreateTime);

        List<ShoppingCart> shoppingCarts = shoppingCartService.list(queryWrapper);

        return R.success(shoppingCarts);
    }

    /**
     * 清空购物车
     * @return
     */
    @DeleteMapping("/clean")
    public R<String> clean(){
        log.info("清空购物车...");
        shoppingCartService.clean();

        return R.success("清空购物车成功！");
    }

    /**
     * 减少购物车菜品或套餐数量
     * @return
     */
    @PostMapping("/sub")
    @Transactional//事务
    public R<String> sub(@RequestBody ShoppingCart shoppingCart){
        log.info("减少购物车数量...");
        //设置用户id，指定当前是哪个用户的购物车数据
        Long userId = BaseContext.getCurrentId();
        shoppingCart.setUserId(userId);

        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,userId);

        //查询当前删除的菜品或者套餐是否在购物车里
        //获取菜品id
        Long dishId = shoppingCart.getDishId();
        if (dishId!=null){
            //减少的是菜品
            queryWrapper.eq(ShoppingCart::getDishId,dishId);
        }else {
            //减少的是套餐
            queryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }

        ShoppingCart shopping = shoppingCartService.getOne(queryWrapper);
        if (shopping!=null){
            //如果存在，查出当前的份数number
            int num = shopping.getNumber();
            if (num>1){
                shopping.setNumber(num-1);
                shoppingCartService.updateById(shopping);
            }else if (num == 1){
                //删除这条数据
                shoppingCartService.removeById(shopping);
            }else {
                return R.error("操作异常");
            }
        }
        return R.success("减少购物车菜品或套餐数量成功！");

    }

}
