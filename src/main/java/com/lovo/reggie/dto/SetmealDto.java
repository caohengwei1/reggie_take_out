package com.lovo.reggie.dto;

import com.lovo.reggie.entity.Setmeal;
import com.lovo.reggie.entity.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
