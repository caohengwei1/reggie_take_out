package com.lovo.reggie.dto;

import com.lovo.reggie.entity.OrderDetail;
import com.lovo.reggie.entity.Orders;
import lombok.Data;

import java.util.List;

/**
 * @author chw
 * @create 2022/12/6
 * 订单带订单明细
 */
@Data
public class OrdersDto extends Orders {
    private List<OrderDetail> orderDetails;

}
