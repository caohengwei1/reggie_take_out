package com.lovo.reggie.dto;

import com.lovo.reggie.entity.Dish;
import com.lovo.reggie.entity.DishFlavor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

/**
 * 菜品扩展类
 * DTO：全称为Data Transfer Object，即数据传输对象，一般用于展示层与服务层之间得到数据传输。
 */
@Data
public class DishDto extends Dish {

    //菜品对应的口味数据
    private List<DishFlavor> flavors = new ArrayList<>();

    private String categoryName;

    private Integer copies;
}
