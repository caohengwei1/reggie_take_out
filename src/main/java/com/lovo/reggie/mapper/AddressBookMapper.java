package com.lovo.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.reggie.entity.AddressBook;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AddressBookMapper extends BaseMapper<AddressBook> {

}
