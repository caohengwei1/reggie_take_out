package com.lovo.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.reggie.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DishMapper extends BaseMapper<Dish> {
}
