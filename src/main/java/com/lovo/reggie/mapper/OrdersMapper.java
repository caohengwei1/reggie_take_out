package com.lovo.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.reggie.entity.Orders;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {
}
