package com.lovo.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lovo.reggie.entity.ShoppingCart;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCart> {
}
